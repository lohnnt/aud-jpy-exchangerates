## AUD-JPY-ExchangeRates

# Motivation
This program is a basic currency exchange rate program that looks at the exchange rate between the Australian Dollar to the Japanese Yen. The reason for this is that it is my dream to travel to Japan one day, but also to get the best deal when exchanging money into Japanese Yen. 

# How to use it
Initially you should run the following programs in the following order. 
1. HistoricalData.py - pull the historical data from the existing api database. 
2. getCurrentRate.py - updates the csv file to include the current day. 
3. visualise.py - generate the visualistion from the csv file. 

# Note
Due to the limitations of the api you can only back date for a year based off the current date. Also when going back you can only do a time range of 8 days. 
