import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import timeit

def main():
    df = pd.read_csv("currencyRates.csv",index_col=0)
    df.plot(y=1, grid=True) # the JPY to AUD rates
    df.plot(y=0, grid=True) # the AUD to JPY rates
    plt.show()


if __name__ == '__main__':
    main()
