import requests
import json
from bs4 import BeautifulSoup
import pandas as pd
from datetime import date
from datetime import datetime

def insertData(data):
    file = pd.read_csv("currencyRates.csv",index_col=0)
    indexList = file.index.values.tolist()
    if not data.index in indexList:
        file = pd.concat([file,data],axis=0)
        file.to_csv("currencyRates.csv",sep=",", encoding="utf-8")
        print(file)
    else:
        print("Currency for today is in the list. Please try again tomorrow :)")

def getData(link):
    # webscrapping the html link
    rate = requests.get(link,timeout=5)
    rateSoup = BeautifulSoup(rate.content,'html.parser')
    x = json.loads(rateSoup.get_text())
    df = pd.DataFrame()
    for key, value in x.items():
        # convert into a dataframe
        col = pd.DataFrame.from_dict(value, orient ='index')
        # append the dataframes together via the horizontal axis.
        df = pd.concat([df, col], axis = 1)
    df.columns = ["AUD_JPY","JPY_AUD"]
    return df

def main():
    currentDateTime = datetime.now()
    date = currentDateTime.strftime("%Y") + "-" + currentDateTime.strftime("%m") + "-" + currentDateTime.strftime("%d")
    link = "https://free.currencyconverterapi.com/api/v6/convert?q=AUD_JPY,JPY_AUD&compact=ultra&date=" + date
    #print(link)
    data = getData(link)
    print(data)
    # append the data into the csvFile
    insertData(data)

if __name__ == '__main__':
    main()
