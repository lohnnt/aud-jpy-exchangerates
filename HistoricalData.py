import requests
import json
from bs4 import BeautifulSoup
import pandas as pd

# The getRates methods takes in a URL link and then scrapes the data off that link
# Note: It will only work for links that are from the free currencyconverterapi site
# It will then convert the data into a dataframe that will be later appended on.
def getRates(link):
    # webscrapping the html link
    rate = requests.get(link)
    rateSoup = BeautifulSoup(rate.content,'html.parser')
    x = json.loads(rateSoup.get_text())
    df = pd.DataFrame()
    for key, value in x.items():
        # convert into a dataframe
        col = pd.DataFrame.from_dict(value, orient ='index')
        # append the dataframes together via the horizontal axis.
        df = pd.concat([df, col], axis = 1)
    df.columns = ["AUD_JPY","JPY_AUD"]
    return df

# Create the csv dataset from the provided websites via webscrapping.
def main():
    link5 = "https://free.currencyconverterapi.com/api/v6/convert?q=AUD_JPY,JPY_AUD&compact=ultra&date=2019-02-07"
    link4 = "https://free.currencyconverterapi.com/api/v6/convert?q=AUD_JPY,JPY_AUD&compact=ultra&date=2019-01-29&endDate=2019-02-06"
    link3 = "https://free.currencyconverterapi.com/api/v6/convert?q=AUD_JPY,JPY_AUD&compact=ultra&date=2019-01-20&endDate=2019-01-28"
    link2 = "https://free.currencyconverterapi.com/api/v6/convert?q=AUD_JPY,JPY_AUD&compact=ultra&date=2019-01-11&endDate=2019-01-19"
    link1 = "https://free.currencyconverterapi.com/api/v6/convert?q=AUD_JPY,JPY_AUD&compact=ultra&date=2019-01-02&endDate=2019-01-10"
    list = [link2, link3,link4,link5 ]
    df = getRates(link1)
    for i in list:
        text = getRates(i)
        # append the dataframes together via the vertical axis
        df = pd.concat([df,text],axis=0)
    print(df)
    df.to_csv("currencyRates.csv",sep=",", encoding="utf-8")

if __name__ == '__main__':
    main()
